/* 
 * J O Blech 2014
 * 
 * Created 24/06/2014
 * 
 * this is an extension of the previous (2) showcase
 * 
 * may need runtime setting -Xss515m or similar,  */

package BeSpaceDExamples
import BeSpaceDCore._;

object VisXML_showcase12 extends CoreDefinitions {

	//factory hall
	def FactoryHall = IMPLIES(Owner("FaxctoryHall"),OccupyBox(50,50,250,150))


	def FireSensor1 = IMPLIES(Owner("FireSensor1"),OccupyPoint(80,100))
	def FireSensor2 = IMPLIES(Owner("FireSensor2"),OccupyPoint(120,100))
	def FireSensor3 = IMPLIES(Owner("FireSensor3"),OccupyPoint(160,100))
	def FireSensor4 = IMPLIES(Owner("FireSensor4"),OccupyPoint(200,100))
	def FireSensor5 = IMPLIES(Owner("FireSensor5"),OccupyPoint(230,100))
	
	def fsdetrange = BIGAND (
		IMPLIES(AND(Event("FireDetection"),Prob(1.0)),OccupyCircle(80,100,10))::
		IMPLIES(AND(Event("FireDetection"),Prob(.95)),OccupyCircle(80,100,90))::		
		
		
		IMPLIES(AND(Event("FireDetection"),Prob(1.0)),OccupyCircle(120,100,10))::
		IMPLIES(AND(Event("FireDetection"),Prob(.95)),OccupyCircle(120,100,90))::	
		
		
		IMPLIES(AND(Event("FireDetection"),Prob(1.0)),OccupyCircle(160,100,10))::
		IMPLIES(AND(Event("FireDetection"),Prob(.95)),OccupyCircle(160,100,90))::	
		
		
		IMPLIES(AND(Event("FireDetection"),Prob(1.0)),OccupyCircle(200,100,10))::
		IMPLIES(AND(Event("FireDetection"),Prob(.95)),OccupyCircle(200,100,90))::	
		
		
		IMPLIES(AND(Event("FireDetection"),Prob(1.0)),OccupyCircle(230,100,10))::
		IMPLIES(AND(Event("FireDetection"),Prob(.95)),OccupyCircle(230,100,90))::	
		Nil)

	def FireExtinguisher = IMPLIES(Owner("FireExtinguisher"),OccupyPoint(100,100))
	
	//machine 1
	def machine1 = IMPLIES(Owner("Machine1"),OccupyBox(60,70,62,72));
		
	def machine1states = BIGOR(
			ComponentState("off")::
			ComponentState("normal")::
			ComponentState("broken")::
			ComponentState("onFire")::			
			Nil
	)
	
	def machine1errorstate = IMPLIES(ComponentState("broken"),Event("ToxicSpillMachine1"));
	def machine1toxicspill = BIGAND (
			IMPLIES(AND(Event("ToxicSpillMachine1"),Prob(0.9)),OccupyCircle(61,71,10))::
			IMPLIES(AND(Event("ToxicSpillMachine1"),Prob(0.1)),OccupyCircle(61,71,20))::

			Nil
	)
	
	//machine 2
	def machine2 = IMPLIES(Owner("Machine2"),OccupyBox(75,85,80,86));
		
	def machine2states = BIGOR(
			ComponentState("off")::
			ComponentState("normal")::
			ComponentState("broken")::
			ComponentState("onFire")::			
			Nil
	)
	
	def machine2errorstate = IMPLIES(ComponentState("broken"),Event("ToxicSpillMachine1"));
	def machine2toxicspill = BIGAND (
			IMPLIES(AND(Event("ToxicSpillMachine2"),Prob(0.9)),OccupyCircle(77,85,15))::
			IMPLIES(AND(Event("ToxicSpillMachine2"),Prob(0.1)),OccupyCircle(77,85,17))::

			Nil
	)	
	
	
	def stafflocation1 = IMPLIES(Owner("Charles"),OccupyPoint(20,20))
	def stafflocation2 = IMPLIES(Owner("Alice"),OccupyPoint(30,25))
	
  def main(args: Array[String]) {
	  println("<output>")
	  println("<command type=\"view\" image=\"substation10.jpg\" rectx=\"" + (Math.random()*200.0 + 200).toInt +"\" recty=\"500\" rectw=\"150\" recth=\"100\"></command>")
	  println("</output>")
  }

 
}