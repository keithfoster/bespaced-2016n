package BeSpaceDExamples

import BeSpaceDCore._

object GraphTest2 extends GraphOperations{


  def exampletrain1 = BIGAND(
      IMPLIES(TimePoint(100),BIGAND(OccupyNode(1)::OccupyNode(2)::OccupyNode(3)::Nil))::
      IMPLIES(TimePoint(101),BIGAND(OccupyNode(2)::OccupyNode(3)::OccupyNode(4)::Nil))::
      IMPLIES(TimePoint(102),BIGAND(OccupyNode(3)::OccupyNode(4)::OccupyNode(5)::Nil))::
      Nil)
     
  def exampletrain2(n : Int): Invariant ={
    var positions : List[Invariant] = Nil
    for (i <- 1 to n) {
      positions ::= IMPLIES (TimePoint(100 + i),BIGAND (OccupyNode(i)::OccupyNode(i+1)::OccupyNode(i+2)::OccupyNode(i+3)::Nil))
    }
    return BIGAND(positions)
  }
  
  
  def main(args: Array[String]) {
    println(exampletrain1)
    println(exampletrain2(10))
  }
  
}