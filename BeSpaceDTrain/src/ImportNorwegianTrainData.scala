/*
 * LICENSED under Apache 2.0 (http://directory.fsf.org/wiki/License:Apache2.0)
 */

import java.io.File
import scala.io.Source
import BeSpaceDCore._

/**
 * @author keith
 */


object ImportNorwegianTrainData {
  
  def main(args: Array[String]): Unit =
  {
    val myFile = new File("/Users/keith/workspace/BeSpaceDTrain/NorwegianExamples/train1.txt")
    val src = Source.fromFile(myFile)
    
    val trackingList: List[Invariant] = (src.getLines map convertTrainTrackingLineToInvariant).toList
    //println(s"trackingList ${trackingList}")
    println(s"length of trackingList ${trackingList.length}")
    
    val invariant = BIGAND(trackingList)
    
//    val output = new CoreDefinitions().prettyPrintInvariant(invariant)
//    println(output)
  }
  
  def convertTrainTrackingLineToInvariant(trackingData: String): Invariant =
  {
    def convertTrainTrackingPointToOccupyNode(position: Int): OccupyNode[Int] =
    {
      OccupyNode(position)
    }
    
    val values = (trackingData split(",")).toList
    
    if (values.length > 0)
    {
       val timepoint = TimePoint(values(0))
       val positions: List[Int] = values.tail map { _.toInt }
       
       val points = positions map convertTrainTrackingPointToOccupyNode
       
       BIGAND(timepoint :: points)
    }
    else
      FALSE()
  }
}