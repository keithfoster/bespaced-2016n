
/*
* Jan Olaf Blech 
* RMIT University
* 2014
*/
package BeSpaceD3D

import java.awt.Color;
import com.sun.j3d.utils.geometry._;
import com.sun.j3d.utils.geometry.GeometryInfo;
import com.sun.j3d.utils.geometry.NormalGenerator;
import com.sun.j3d.utils.universe.SimpleUniverse;
import com.sun.j3d.utils.geometry.Box;
import javax.media.j3d._;
import javax.vecmath._;
import BeSpaceDCore._;


object Visualization3D extends App {
    
	var inv : Invariant = FALSE()
	
	//val xoffset,yoffset : Int = 0;
    
    def setInvariant (inv : Invariant){
        this.inv = inv
    }
	

    
 //var box :Box = new Box();
 
 var boxlist : List[Box] = Nil;
 var universe : SimpleUniverse = new SimpleUniverse();
 var group : BranchGroup = new BranchGroup();    

 	def handleInv (appearance : Appearance, group : Group, inv : Invariant) {
 	  	val xoffset: Int = 0;
 		val yoffset : Int = 0;
    	  inv match {
        	 case (AND(inv1,inv2)) => {
        		 handleInv(appearance, group, inv1)
        		 handleInv(appearance, group, inv2)
        	 }
        	 case (BIGAND(inv1::l)) => {
        		 handleInv(appearance, group, inv1)
        		 handleInv(appearance, group, BIGAND(l))
        	 }
        	 // JO do this later
        	 //case (Occupy3DSegment(x1,y1,x2,y2,r)) => {
   
        	 //}  
          	 case (Occupy3DBox(x1,y1,z1,x2,y2,z2)) => {
          	    val x1o = x1 + xoffset
          	    val x2o = x2 + xoffset
          	    val y1o = y1 + yoffset
          	    val y2o = y2 + yoffset
          	   	var tg : TransformGroup = new TransformGroup();

          	 	var transform :Transform3D = new Transform3D();

          	 	//var vector : Vector3f = new Vector3f( x1.toFloat/1000f-(x2-x1).toFloat/2000f, y1.toFloat/1000f-(y2-y1).toFloat/2000f, z1.toFloat/1000f-(z2-z1).toFloat/2000f);
          	 	var vector : Vector3f = new Vector3f( (x1o.toFloat/500f)+((x2o-x1o).toFloat/1000f), (y1o.toFloat/500f)+((y2o-y1o).toFloat/1000f), (z1.toFloat/500f)+((z2-z1).toFloat/1000f));

          	 	transform.setTranslation(vector);

          	 	tg.setTransform(transform);
          	 	var box = new Box ((x2o-x1o).toFloat/1000f,(y2o-y1o).toFloat/1000f,(z2-z1).toFloat/1000f,appearance)

          	 	tg.addChild(box);

          	 	group.addChild(tg)
        		// boxlist ::= new Box();
          	 }	  
        	 case _ => 
    	  } 

    }
  
  def doGraphics() {
  		
		//group.addChild(new Sphere(0.5f));

		universe = new SimpleUniverse();
		
		val invariantvis1 : QuadArray = new QuadArray(360,1);
		
		

	
		val geometryInfo :GeometryInfo = new GeometryInfo(invariantvis1);
		//geometryInfo.
		val ng : NormalGenerator = new NormalGenerator();
		ng.generateNormals(geometryInfo);

		val result : GeometryArray = geometryInfo.getGeometryArray();
		val appearance  : Appearance = new Appearance();
		val color : Color3f = new Color3f(Color.yellow);
		val black : Color3f = new Color3f(0.0f, 0.0f, 0.0f);
		val white : Color3f = new Color3f(1.0f, 1.0f, 1.0f);
		val texture : Texture = new Texture2D();
		val texAttr : TextureAttributes = new TextureAttributes();
		texAttr.setTextureMode(TextureAttributes.MODULATE);
		texture.setBoundaryModeS(Texture.WRAP);
		texture.setBoundaryModeT(Texture.WRAP);
		texture.setBoundaryColor(new Color4f(0.0f, 1.0f, 0.0f, 0.5f));
		val mat : Material = new Material(color, black, color, white, 70f);
		appearance.setTextureAttributes(texAttr);
		appearance.setMaterial(mat);
		appearance.setTexture(texture);
		val ta : TransparencyAttributes = new TransparencyAttributes();
		ta.setTransparencyMode(1);
		ta.setTransparency(0.5f);
		
		appearance.setTransparencyAttributes(ta);
		val shape : Shape3D = new Shape3D(result, appearance);
		group  = new BranchGroup();
		//group.addChild(shape);
		
		handleInv(appearance,group,inv);
		/*
		for (i <- 0 to 10) {
			var tg : TransformGroup = new TransformGroup();

        	var transform :Transform3D = new Transform3D();

        	var vector : Vector3f = new Vector3f( 0f, .5f, .1f*i);

			transform.setTranslation(vector);

			tg.setTransform(transform);
			var box = new Box (.1f,.1f,.1f,appearance)

			tg.addChild(box);

			group.addChild(tg)
  		}
  		* 
  		* 
  		*/
		/*
				val tg2 : TransformGroup = new TransformGroup();

        val transform2 :Transform3D = new Transform3D();

        val vector2 : Vector3f = new Vector3f( .5f, .0f, .0f);

      transform2.setTranslation(vector2);

      tg2.setTransform(transform2);
		val box2 = new Box (.1f,.1f,.1f,appearance)

      tg2.addChild(box2);
		// box = new Box (result,appearance)

		group.addChild(tg2)
	*/
		val viewTranslation : Vector3f = new Vector3f();
		viewTranslation.z = 3f;
		viewTranslation.x = 0f;
		viewTranslation.y = 0f;
		val viewTransform : Transform3D  = new Transform3D();
		viewTransform.setTranslation(viewTranslation);
		val rotation : Transform3D = new Transform3D();
		rotation.rotX(-Math.PI / 3.1d / 2.5d);
		rotation.rotY(-Math.PI / 3.1d / 2.4d);
		rotation.rotZ(-Math.PI/ 3.1d / 2.4d)
		
		rotation.mul(viewTransform);
		universe.getViewingPlatform().getViewPlatformTransform().setTransform(
				rotation);
		universe.getViewingPlatform().getViewPlatformTransform().getTransform(
				viewTransform);
		
		
		val bounds : BoundingSphere = new BoundingSphere(new Point3d(0.0, 0.0, 2.0),1000.0);
		val light1Color : Color3f  = new Color3f(.8f, .8f, .8f);
		val light1Direction : Vector3f  = new Vector3f(-10.0f, -7.0f, -4.0f);
		val light1 : DirectionalLight  = new DirectionalLight(light1Color, light1Direction);
		light1.setInfluencingBounds(bounds);
		group.addChild(light1);
		val ambientColor : Color3f  = new Color3f(1f, 1f, .1f);
		val ambientLightNode : AmbientLight  = new AmbientLight(ambientColor);
		ambientLightNode.setInfluencingBounds(bounds);
		group.addChild(ambientLightNode);
		
		universe.addBranchGraph(group);
		//universe.getCanvas().
  }
  
  def viewUpdate(xval: Double, yval : Double, zval : Double) {
    //int x = box.getXdimension();
    println("x")
    /*
    universe = new SimpleUniverse()
    //group.removeChild(box)
    box = new Box (.5f,.5f,.5f,new Appearance())
    group = new BranchGroup()
    
    val viewTranslation : Vector3f = new Vector3f();
		viewTranslation.z = 3;
		viewTranslation.x = 0f;
		viewTranslation.y = 0f;
		val viewTransform : Transform3D  = new Transform3D();
		viewTransform.setTranslation(viewTranslation);
		val rotation : Transform3D = new Transform3D();
		rotation.rotX(-Math.PI / 3.1d);
		rotation.rotY(-Math.PI / 3.1d);
		rotation.rotZ(-Math.PI/ 3.1d)
		
		rotation.mul(viewTransform);
		universe.getViewingPlatform().getViewPlatformTransform().setTransform(
				rotation);
		universe.getViewingPlatform().getViewPlatformTransform().getTransform(
				viewTransform);
		
		
		val bounds : BoundingSphere = new BoundingSphere(new Point3d(0.0, 0.0, 2.0),1000.0);
		val light1Color : Color3f  = new Color3f(.8f, .8f, .8f);
		val light1Direction : Vector3f  = new Vector3f(-10.0f, -7.0f, -4.0f);
		val light1 : DirectionalLight  = new DirectionalLight(light1Color, light1Direction);
		light1.setInfluencingBounds(bounds);
		group.addChild(light1);
		val ambientColor : Color3f  = new Color3f(1f, 1f, .1f);
		val ambientLightNode : AmbientLight  = new AmbientLight(ambientColor);
		ambientLightNode.setInfluencingBounds(bounds);
		group.addChild(ambientLightNode);
    group.addChild(box)
    universe.addBranchGraph(group)
*/
    val viewTranslation : Vector3f = new Vector3f();
		viewTranslation.z = 3f;
		viewTranslation.x = 0f // 0.5f;
		viewTranslation.y = 0f //6.1f;
		val viewTransform : Transform3D  = new Transform3D();
		viewTransform.setTranslation(viewTranslation);
		val rotation : Transform3D = new Transform3D();
		rotation.rotX(-Math.PI / xval);
		//rotation.rotY(-Math.PI / 1);
		//rotation.rotZ(-Math.PI/ 1)
		
		rotation.mul(viewTransform);
		universe.getViewingPlatform().getViewPlatformTransform().setTransform(
				rotation);
		universe.getViewingPlatform().getViewPlatformTransform().getTransform(
				viewTransform);
    
    
  }
  
  override def main(args: Array[String]) {
    var xval = 5.0d;
    var dir : Boolean = true;
    doGraphics();
    for (i <- 0 to 10000) {
    Thread.sleep(10);
    
    viewUpdate(xval,1,1);
    if (xval >= 12d || xval <= 1d) dir = ! dir;
    if (dir)
    	xval += 0.01d;
    else
    	xval -= 0.01d
    }
 

		
  }
}