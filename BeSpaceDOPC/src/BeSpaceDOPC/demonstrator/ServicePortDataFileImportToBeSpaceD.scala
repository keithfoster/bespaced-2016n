/*
 * LICENSED under Apache 2.0 (http://directory.fsf.org/wiki/License:Apache2.0)
 */

/**
 * @author keith
 */


package BeSpaceDOPC.demonstrator

import java.io.File
//import java.util.List

// BeSPaceD Core
import BeSpaceDCore._

// Service Port
import BeSpaceDOPC.serviceport._



object ServicePortDataFileImportToBeSpaceD {
      

  def main(args: Array[String]) 
  {
    // REAL TEST DATA from ABB
    // Note: This file is not checked into the repository
    //       So you'll need to use the dummy test file instead
    
    val dataFilename = "data/ABB - Example Raw Data File.txt"
    //val dataFilename = "data/DummmyDataFile.txt"

    
    // Create a parser function specific to this file
    
//    println("PARSE ABB ServicePort datafile (big one) using simple elemnet conversion")
//    println("========================================================================")
//    parseDatafile(dataFilename, dataLoggerElementTransformer)
    
    println("PARSE ABB ServicePort datafile (big one) using better whole sample conversion")
    println("=============================================================================")
    parseDatafile2(dataFilename, dataLoggerElementParser, dataLoggerSampleTransformer)

  }

}